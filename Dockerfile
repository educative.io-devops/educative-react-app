# Use an official Node runtime as a parent image
FROM node:20.9.0-alpine

# Set the working directory in the container
WORKDIR /usr/src/app

# Copy package.json and package-lock.json (or yarn.lock if you use yarn)
COPY package*.json ./

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
RUN npm install

# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

# Build the app
# RUN npm run build

# The base image for this will be an Nginx image to serve the built React app
# FROM nginx:stable-alpine

# Copy the build output to replace the default nginx contents.
# COPY --from=0 /usr/src/app/build /usr/share/nginx/html

# Expose port 80 to the outside once the container has launched
EXPOSE 3000

# Define the command to run the app using CMD which defines your runtime
# CMD ["nginx", "-g", "daemon off;"]

CMD [ "npm", "start" ]